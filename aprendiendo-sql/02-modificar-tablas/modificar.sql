ALTER TABLE usuarios RENAME TO usuarios_renamed;

ALTER TABLE usuarios_renamed CHANGE apellidos apellido varchar(100) null;

ALTER TABLE usuarios_renamed MODIFY apellido char(50) not null;

ALTER TABLE usuarios_renamed ADD website  varchar(100) null;

ALTER TABLE usuarios_renamed ADD CONSTRAINT  uq_email UNIQUE(email);

ALTER TABLE usuarios_renamed DROP website;