desc 
/* 
 * listar los encargos con el nombre del coche e] nombre del cliente,
y el nombre de la ciudad, pero unicamente cuando sean de madrid.
 */

select  e.id,
        co.marca as coche,
        cl.nombre as cliente,
        cl.ciudad
from    encargos e
join    clientes cl
  on    cl.id = e.clientes_id
 and    cl.ciudad like '%ba%'
join    coches co
  on    co.id = e.coches_id;