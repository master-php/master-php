use concesionario;
/* 
 * incrementar el precio de todos los coches en un 5%.
 */
select  modelo,
        precio
from    coches;

update  coches
set     precio = precio * 1.05;
