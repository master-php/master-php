use concesionario;
/* 
 obtener listado de clientes mostrando el id, nombre y vendedor id 
con su nombre. 
 */

select  cl.id as 'cliente id',
        cl.nombre 'cliente',
        v.id as 'vendedor id',
        concat(v.nombre, ' ', v.apellidos) as vendedor
from    clientes cl
join    vendedores v
        on v.id = cl.vendedores_id;