use concesionario;
/* 
 * listar los clientes con encargos sobre el mercedes ranchera.
 */

select  cl.nombre as cliente,
        e.id as 'No. encargo',
        e.cantidad        
from    encargos e
join    clientes cl
  on    cl.id = e.clientes_id
where   e.coches_id in (
                        select  id
                        from    coches
                        where   modelo like 'merce%');