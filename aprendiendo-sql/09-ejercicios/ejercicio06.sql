use concesionario;
/* 
 * visualizar el nombre y los apellidos de los vendedores
en una sola columna y su fecha de registro y el dia 
de la semana en la cual se registraron.
 */

select  concat(nombre, ' ', apellidos) as 'nombre vendedor',
        fecha,
        dayname(fecha) as 'dia de la semana'
from    vendedores;