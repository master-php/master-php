use concesionario;
/* 
 * obtener listado de clientes atendidos por el vendedor David lopez.
 */

update  clientes
set     vendedores_id = 4
where   id = 3;

update  clientes
set     vendedores_id = 4
where   id = 5;

update  clientes
set     vendedores_id = 2
where   id = 6;

select  nombre
from    clientes
where   vendedores_id = 1;

select  nombre
from    clientes
where   vendedores_id in(
                        select  id
                        from    vendedores
                        where   nombre like 'david%');