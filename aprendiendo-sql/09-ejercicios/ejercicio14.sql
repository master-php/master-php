use concesionario;
/* 
 * visualizar el total de unidades vendidas de cada coche a cada cliente,
mostrando el nombre del producto, numero de cliente y total de unidades.
 */

select  concat(co.modelo, ' ', co.marca) as coche,
        cl.nombre as cliente,
        en.cantidad
from    encargos en
join    coches co
on      co.id = en.coches_id
join    clientes cl
on      cl.id = en.clientes_id
group by en.coches_id, en.clientes_id;