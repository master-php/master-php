use concesionario;
/* 
 * mostrar los datos del vendedor con mas antiguedad en el concesionario.
 */

select  *
from    vendedores
order by fecha asc limit 1;

#30 plus: obtener los coches con mas unidades vendidas.
select  c.modelo as coche,
        sum(e.cantidad) as 'total vendidos'
from    encargos e
join    coches c
  on    c.id = e.coches_id
group by e.coches_id
order by 2 desc limit 3;