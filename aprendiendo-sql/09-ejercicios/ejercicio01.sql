create database if not exists concesionario;

use concesionario;

create table coches (
id int(10)auto_increment not null,
modelo varchar(100) not null,
marca varchar(50),
precio int(20) not null,
stock int(255) not null,
constraint pk_coches primary key(id)
)engine=InnoDb;

create table grupos (
id int(10)auto_increment not null,
nombre varchar(100) not null,
ciudad varchar(100),
constraint pk_grupos primary key(id)
)engine=InnoDb;

create table vendedores (
id int(10)auto_increment not null,
grupos_id int(10) not null,
jefe int(10),
nombre varchar(100) not null,
apellidos varchar(150),
cargo varchar(50),
fecha date,
sueldo float(20, 2),
comision float(10, 2),
constraint pk_vendedores primary key(id),
constraint fk_vendedor_grupo foreign key(grupos_id) references grupos(id),
constraint fk_vendedor_jefe foreign key(jefe) references vendedores(id)
)engine=InnoDb;

create table clientes (
id int(10)auto_increment not null,
vendedores_id int(10),
nombre varchar(150) not null,
ciudad varchar(100),
gastado float(50, 2),
fecha date,
constraint pk_clientes primary key(id),
constraint fk_cliente_vendedor foreign key(vendedores_id) references vendedores(id)
)engine=InnoDb;

create table encargos (
id int(10)auto_increment not null,
clientes_id int(10) not null,
coches_id int(10) not null,
cantidad int(100),
fecha date,
constraint pk_encargos primary key(id),
constraint fk_encargo_cliente foreign key(clientes_id) references clientes(id),
constraint fk_encargo_coche foreign key(coches_id) references coches(id)
)engine=InnoDb;

#insert
insert  into coches
values  (null, 'renault clio', 'renault', 12000, 13),
        (null, 'seat panda', 'seat', 10000, 10),
        (null, 'mercedes ranchera', 'mercedes benz', 32000, 24),
        (null, 'porche cayena', 'porche', 65000, 5),        
        (null, 'lambo aventador', 'lamborgini', 170000, 2),
        (null, 'ferrari spider', 'ferrari', 245000, 80);
     
insert  into grupos
values  (null, 'vendedores A', 'madrid'),
        (null, 'vendedores B', 'madrid'),
        (null, 'directores mecanicos', 'madrid'),
        (null, 'vendedores A', 'barcelona'),
        (null, 'vendedores B', 'barcelona'),
        (null, 'vendedores C', 'valencia'),
        (null, 'vendedores A', 'bilbao'),
        (null, 'vendedores B', 'pamplona'),
        (null, 'vendedores C', 'santiago de compostela');

insert  into vendedores
values  (null, 1, null, 'david', 'lopez', 'responsable de tienda', curdate(), 30000.0, 4.0),
        (null, 1, 1, 'fran', 'martinez', 'ayudante en tienda', curdate(), 23000.0, 2.0),
        (null, 2, null, 'nelson', 'sanchez', 'responsable de tienda', curdate(), 38000.0, 5.0),
        (null, 2, 3, 'jesus', 'lopez', 'ayudante en tienda', curdate(), 12000.0, 6.0),
        (null, 3, null, 'victor', 'lopez', 'mecanico jefe', curdate(), 50000.0, 2.0),
        (null, 4, null, 'antonio', 'lopez', 'vendedor de recambios', curdate(), 13000.0, 8.0),
        (null, 5, null, 'salvador', 'lopez', 'vendedor experto', curdate(), 60000.0, 2.0),
        (null, 6, null, 'joaquin', 'lopez', 'ejecutivo de cuentas', curdate(), 80000.0, 1.0),
        (null, 6, 8, 'luis', 'lopez', 'ayudante en tienda', curdate(), 10000.0, 10.0);

insert  into clientes
values  (null, 1, 'construcciones diaz inc', 'alcobendas', 24000, curdate()),
        (null, 1, 'fruteria antonia inc', 'fuenlabrada', 40000, curdate()),
        (null, 1, 'imprenta martinez inc', 'barcelona', 32000, curdate()),
        (null, 1, 'jesus colchones inc', 'el prat', 96000, curdate()),
        (null, 1, 'bar pepe inc', 'valencia', 170000, curdate()),
        (null, 1, 'tienda pc inc', 'murcia', 245000, curdate());

insert  into encargos
values  (null, 1, 1, 2, curdate()),
        (null, 2, 2, 4, curdate()),
        (null, 3, 3, 1, curdate()),
        (null, 4, 3, 3, curdate()),
        (null, 5, 5, 1, curdate()),
        (null, 6, 6, 1, curdate());


select  en.cantidad,
        cl.nombre as cliente
from    encargos en
join    clientes cl
on      cl.id = en.clientes_id;  

select  en.cantidad,
        cl.nombre as cliente,
        co.modelo
from    encargos en
join    clientes cl
on      cl.id = en.clientes_id
join    coches co
on      co.id = en.coches_id;