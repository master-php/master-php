use concesionario;
/* 
 * mostrar los clientes q mas pedidos han hecho y mostrar cuantos hicieron. 
 */

insert  into encargos
values  (null, 4, 4, 3, curdate());

select  cl.nombre as cliente,
        count(en.id) as 'cantidad de pedidos'
from    encargos en
join    clientes cl
on      cl.id = en.clientes_id
group by en.clientes_id
order by 2 desc
limit   2;