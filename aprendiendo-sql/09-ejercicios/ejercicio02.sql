use concesionario;
/* modificar comision de los vendedores al 0.5%
      cuando ganan 50000 o mas.
*/
select  concat(ve.nombre, ' ', ve.apellidos) as vendedor,
        ve.comision
from    vendedores ve
where   ve.sueldo >= 50000;  

update  vendedores ve
set     ve.comision = 0.5
where   ve.sueldo >= 50000;