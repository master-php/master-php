use concesionario;
/* 
 * sacar los vendedores q tienen jefe incluyendo
el nombre del jefe.
 */
select  v1.id,
        concat(v1.nombre, " ", v1.apellidos) as vendedor,
        concat(v2.nombre, " ", v2.apellidos) as jefe
from    vendedores v1
join    vendedores v2
  on    v2.id = v1.jefe;
