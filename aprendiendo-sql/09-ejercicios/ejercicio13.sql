use concesionario;
/* 
 * sacar la media de sueldos y el nombre del grupo entre los vendedores por grupo.
 */

select  concat(gr.nombre, ' ', gr.ciudad) as grupo,  
        round(avg(ve.sueldo), 2) as 'promedio salarial'
from    vendedores ve
join    grupos gr
on      gr.id = ve.grupos_id
group by ve.grupos_id;

select  concat(gr.nombre, ' ', gr.ciudad) as grupo,  
        ceil(avg(ve.sueldo)) as 'promedio salarial'
from    vendedores ve
join    grupos gr
on      gr.id = ve.grupos_id
group by ve.grupos_id;