use concesionario;
/* 
 * sacar a todos los vendedores con cuya fecha de alta sea posterior 
   a jul/1/2018.
 */
alter table vendedores
change  fecha_alta fecha date;

update  vendedores
set     fecha = '2018-08-08';

select  concat(nombre, ' ', apellidos) as 'nombre vendedor',
        fecha
from    vendedores
where   fecha > '2018-07-01';

update  vendedores
set     fecha = '2017-04-03'
where   id = 8;