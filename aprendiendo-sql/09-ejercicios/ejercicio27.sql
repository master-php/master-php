use concesionario;
/* 
 * visualizar los nombres de los clients y la cantidad de
encargos realizados incluyendo los que no tengan encargos. 
 */

insert into clientes
values(null, 5, 'tienda organica inc.', 
        'murcia', 0, curdate());

select  e.id as encargo, 
        cl.nombre,
        count(e.id) as encargos
from    encargos e
right join clientes cl
  on    cl.id = e.clientes_id
group by e.clientes_id 
order by e.clientes_id;

/* much better*/
select  cl.nombre,
        count(e.id) as encargos
from    clientes cl
left join encargos e
  on    cl.id = e.clientes_id
group by cl.nombre;