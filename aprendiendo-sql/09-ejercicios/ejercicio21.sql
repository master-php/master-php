use concesionario;
/* 
 * obtener nombres y ciudades de clientes con encargos mayores a 2 coches.
 */

select  nombre,
        ciudad
from clientes
where   id in (
                select  clientes_id
                from    encargos
                where   cantidad > 2);