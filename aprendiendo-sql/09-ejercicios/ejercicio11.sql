use concesionario;
/* 
 * Visualizar todos los cargos y el numero de
vendedores por cargo. 
 */
select  cargo,
        count(id)
from    vendedores
group by cargo;
