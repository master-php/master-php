use concesionario;
/* 
 * mostrar todos los vendedores del grupo 2, 
ordenados por salario en forma descendente.
 */

select  *
from    vendedores
where   grupos_id = 2
order by sueldo desc;