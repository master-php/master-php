use concesionario;
/* 
 * seleccionar el grupo en el q trabaja el vendedor con el
maximo salario y mostrar el grupo.
 */
select  concat(ve.nombre, ' ', ve.apellidos) as vendedor,
        g.nombre,
        g.ciudad
from    vendedores ve
join    grupos g
  on    g.id = ve.grupos_id    
where   ve.sueldo in (
                select  max(sueldo)
                from    vendedores);