use concesionario;
/* 
 * mostrar todos los vendedores con su nombre y el numero
de dias q llevan en el concesionario.
 */

select  concat(nombre, ' ', apellidos) as 'nombre vendedor',
        datediff('2018-08-08', fecha) as 'dias'
from    vendedores;

update  vendedores
set     fecha = '2017-12-03'
where   id = 4;