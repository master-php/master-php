use concesionario;
/* 
 * listar los vendedores tengan o no clientes y mostrar el numero de cliente.
 */

select  concat(v.nombre, " ", v.apellidos) as vendedor,
        count(cl.id) as 'clientes'
from    vendedores v
left join clientes cl
       on cl.vendedores_id = v.id
group by v.id;