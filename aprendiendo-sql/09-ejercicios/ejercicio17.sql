use concesionario;
/* 
 * obtener listado con los encargos realizados por el cliente fruteria antonia inc.
 */

select  e.id as 'No. encargo',
        e.cantidad,
        c.nombre as cliente,
        co.modelo as modelo,        
        e.fecha
from    encargos e
join    clientes c
  on    c.id = e.clientes_id
join    coches co
  on    co.id = e.coches_id
where   e.clientes_id in(
                        select  id
                        from    clientes
                        where   nombre like 'fru%');