use concesionario;
/* 
 * visualizar los apellidos de los vendedores, su fecha y
su numero de grupo, ordenado por fecha descendente. 
Limitando a mostrar los 4 ultimos.
 */
select  apellidos,
        fecha,
        id
from    vendedores
order by fecha desc
limit   4;
