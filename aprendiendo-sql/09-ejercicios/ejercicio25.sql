use concesionario;
/* 
 * obtener una lista de los clientes con el importe acumulado.
 */

select  cl.nombre as cliente,        
        sum(e.cantidad * c.precio) as 'importe'
from    encargos e
join    coches c
  on    c.id = e.coches_id
join    clientes cl
  on    cl.id = e.clientes_id
group by cl.nombre
order by 2 asc;