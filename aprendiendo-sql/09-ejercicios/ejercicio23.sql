use concesionario;
/* 
 * listar todos los encargos realizados con la marca del coche
y el nombre del cliente.
 */
select  e.id,
        co.marca,
        cl.nombre as cliente
from    encargos e
join    clientes cl
  on    cl.id = e.clientes_id
join    coches co
  on    co.id = e.coches_id;
