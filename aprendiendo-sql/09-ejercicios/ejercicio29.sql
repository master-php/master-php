use concesionario;
/* 
 * crear una vista llamada vendoresA que incluira todos los vendores 
del grupo "vendedores A"

 */
create view vendedoresA as
                            select  *
                            from    vendedores v
                            where   v.grupos_id in (
                                    select  g.id
                                    from    grupos g
                                    where   g.nombre = 'vendedores A');
                             
