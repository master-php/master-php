use concesionario;
/* 
 * visulizar todos los coches en cuya marca exista
la letra 'A' y cuyo modelo empiece por 'F'.
 */

select  marca,
        modelo
from    coches
where   marca like '%a%'
  and   modelo like 'f%';