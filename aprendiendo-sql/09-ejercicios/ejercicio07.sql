use concesionario;
/* 
 * mostrar nombre y salario de los vendedores con 
cargo de ayudante de tienda.
 */

select  concat(nombre, ' ', apellidos) as 'nombre vendedor',
        sueldo
from    vendedores
where   cargo = 'ayudante en tienda';

select  concat(nombre, ' ', apellidos) as 'nombre vendedor',
        sueldo
from    vendedores
where   cargo like 'ayu%';