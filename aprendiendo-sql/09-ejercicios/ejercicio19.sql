use concesionario;
/* 
 * obtener los vendedores con 2 o mas clientes.
 */

select  cl.vendedores_id,
        concat(ve.nombre, ' ', ve.apellidos) as vendedor,
        count(cl.id) as 'total clientes'
from    clientes cl
join    vendedores ve
  on    ve.id = cl.vendedores_id    
group by cl.vendedores_id
having  count(cl.id) >= 2;
        