use blog_master;
select  titulo
from    entradas;

select  titulo,
        categoria_id
from    entradas
group by categoria_id;

select  count(titulo) as 'numero de entradas',
        categoria_id
from    entradas
group by categoria_id;

select  count(titulo) as 'numero de entradas',
        categoria_id
from    entradas
group by categoria_id
having count(titulo) >= 4;

/* special functions
avg:    for average.
count:  count the numbre of elements.
max:    for the group maximum.
min:    for the group minimum.
sum:    the group acum.
*/

select  avg(id) as 'media de entradas'
from    entradas;

select  max(id) as 'maximo id',
        titulo
from    entradas;

select  min(id) as 'maximo id',
        titulo
from    entradas;

select  sum(id) as 'maximo id',
        titulo
from    entradas;