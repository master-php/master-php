use blog_master;

insert into categorias
values  (null, 'Accion'),
        (null, 'Rol'),
        (null, 'Deportes');

insert into entradas
values  (null, 1, 1, 'novedades de gta 5 online', 'review del gta 5', curdate()),
        (null, 1, 2, 'review de lol online', 'todo sobre el lol', curdate()),
        (null, 1, 3, 'nuevos jugadores de fifa 19', 'review del fifa 19', curdate()),
        (null, 2, 1, 'novedades de assasins', 'review del asssains', curdate()),
        (null, 2, 2, 'review de wow online', 'todo sobre el wow', curdate()),
        (null, 2, 3, 'nuevos jugadores de pes 19', 'review del pes 19', curdate()),
        (null, 3, 1, 'novedades de call of duty online', 'review del cod', curdate()),
        (null, 3, 1, 'review de fortnite online', 'todo sobre el fornite', curdate()),
        (null, 3, 3, 'nuevos jugadores de formula 1 2020', 'review del formula 1 2020', curdate());