use blog_master;
insert into usuarios
values(null, 'Admin', 'Admin', 
       'admin@admin.com', 'admin', curdate()); 

select  *
from    usuarios
where   id in(
                select  usuario_id
                from    entradas);

insert into entradas
values(null, 3, 1, 'Guia de GTA Vice City', 
        'GTA', curdate());

select  nombre,
        apellidos
from    usuarios
where   id in(
                select  usuario_id
                from    entradas
                where   titulo like '%GTA%');

#sacar todas las entradas de la categoria accion utilizando su nombre#
select  *
from    entradas
where   categoria_id in(
                select  id
                from    categorias
                where   nombre = 'Accion');

#mostrar las categorias con mas de 3 entradas#
#option 1#
select  *
from     categorias
where    id in(
                select  categoria_id
                from    entradas
                group by categoria_id
                having  count(categoria_id) > 3);
#option 2#
select  categoria_id,
        count(categoria_id)
from    entradas
group by categoria_id
having  count(categoria_id) > 3;

#mostrar los usuarios q crearon un entrada un martes#
select  *
from     usuarios
where    id in(
                select  usuario_id
                from    entradas
                where   dayname(fecha) = 'Tuesday');

#mostrar nombre del usuario con mas entradas#
select  concat(nombre, ' ', apellidos) as 'usuario con mas entradas'
from     usuarios
where    id = (
                select  usuario_id
                from    entradas
                group by usuario_id
                order by count(id) desc limit 1);

#mostrar las categorias sin entradas#
insert into categorias
values  (null, ';plataformas');

select  *
from     categorias
where    id not in(
                select  categoria_id
                from    entradas);