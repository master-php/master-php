use blog_master;

create view entradas_con_nombres as
    select  en.id,
            en.titulo,
            concat(us.nombre, ' ', us.apellidos) as 'author',
            ca.nombre as 'categoria'
    from    entradas en
    join    usuarios us
    on      us.id = en.usuario_id
    join    categorias ca
    on      ca.id = en.categoria_id;

show tables;

select  *
from    entradas_con_nombres;

select  *
from    entradas_con_nombres
where   author like('Dia%');

drop view entradas_con_nombres;