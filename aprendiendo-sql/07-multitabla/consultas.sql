use blog_master;

select  en.id,
        en.titulo,
        concat(us.nombre, ' ', us.apellidos) as 'author',
        ca.nombre as 'categoria'
from    entradas en
join    usuarios us
on      us.id = en.usuario_id
join    categorias ca
on      ca.id = en.categoria_id;

#exercises#
#mostrar nombre de las categorias y cuantas entradas#
#hay por categoria#
update  categorias
set     nombre = 'plataformas'
where   id = 4;

select  c.nombre as 'categoria',
        count(c.id) as 'total'
from    categorias c
join    entradas e
on      e.categoria_id = c.id
group by c.nombre;

#mostrar el email de los usuarios y #
#cuantas entradas hay#
select  u.email,
        count(u.email) as total
from    usuarios u
join    entradas e
on      e.usuario_id = u.id
group by u.email;

#left join
#mostrar nombre de las categorias y cuantas entradas#
#hay por categoria#
select  c.nombre as 'categoria',
        count(e.id) as 'total',
        c.id
from    categorias c
left join entradas e
on      e.categoria_id = c.id
group by e.categoria_id;

#right join
select  c.nombre as 'categoria',
        count(e.id) as 'total',
        c.id
from    entradas e
right join categorias c 
on      e.categoria_id = c.id
group by e.categoria_id;
