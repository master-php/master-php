use blog_master;
#consulta with a one condition.

select  *
from    usuarios
where   email = 'oa@oa.com';

select  *
from    usuarios
where   month(fecha) = 6;

select  email
from    usuarios
where   apellidos like '%a%';

select  nombre,
        year(fecha)
from    usuarios
where   (year(fecha) % 2 = 0);

select  upper(nombre) nombre,
        year(fecha)
from    usuarios
where   length(nombre) > 10
  and   year(fecha) < 2021;

select  *
from    usuarios
order by id;

select  *
from    usuarios
order by idselect  *
from    usuarios
limit   0,4; asc;

select  *
from    usuarios
order by id desc;

select  *
from    usuarios
limit   1;

select  *
from    usuarios
limit   0,4;