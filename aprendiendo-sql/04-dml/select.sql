#show some table columns 
select  email,
        nombre,
        apellidos
from usuarios;

#show all table columns
select  *
from usuarios;

#math operators
select  email, 
        (4+7)
from    usuarios;

select  email, 
        (4+7) as operation
from    usuarios;

select  id,
        email, 
        (4+7) as operation
from    usuarios
order by id;

select  id,
        email, 
        (4+7) as operation
from    usuarios
order by id desc;

select  id,
        email, 
        (id+7) as operation
from    usuarios;

#math functions
select  abs(5 - 7) as operation
from    usuarios;

select  ceil(7.34) as operation
from    usuarios;

select  floor(7.34) as operation
from    usuarios;

select  pi() as operation
from    usuarios;

select  rand() as operation
from    usuarios;

select  round(7.91) as operation
from    usuarios;

select  round(7.91, 1) as operation1,
        round(7.91, 2) as operation2
from    usuarios;

select  sqrt(7.91) as operation
from    usuarios;

select  truncate(7.91, 1) as operation1,
        truncate(7.91, 0) as operation2
from    usuarios;

select  round(id, 2) as operation1
from    usuarios;

#string functions
select  upper(nombre) as nombre
from    usuarios;

select  lower(nombre) as nombre
from    usuarios;

select  concat(nombre, ' ', apellidos) as nombre
from    usuarios;

select  concat('   ', nombre, ' ', apellidos, '   ') as nombre
from    usuarios;

select  upper(concat(nombre, ' ', apellidos)) as nombre
from    usuarios;

select  length(concat(nombre, ' ', apellidos)) as characters
from    usuarios;

select  trim(concat('   ', nombre, ' ', apellidos, '   ')) as nombre
from    usuarios;

//date & time functions
show databases;
select fecha
from    usuarios;

select  email,
        fecha
from    usuarios;

select  email,
        fecha,
        curdate() as "today's date"
from    usuarios;

select  email,
        dayname(fecha) as "today's day"
from    usuarios;

select  email,
        dayofmonth(fecha) as "day of the month"
from    usuarios;

select  email,
        dayofweek(fecha) as "day of the week"
from    usuarios;

select  email,
        dayofyear(fecha) as "day of the year"
from    usuarios;

select  email,
        fecha,
        month(fecha) as "month",
        day(fecha) as "day",
        year(fecha) as "year",
        hour(fecha) as "horas",
        minute(fecha) as "minutes",
        second(fecha) as "seconds"
from    usuarios;

select  email,
        fecha,
        curtime() as "current time"
from    usuarios;

select  email,
        fecha,
        sysdate() as "current time"
from    usuarios;

select  email,
        date_format(fecha, '%d/%m/%Y') as fecha,
        sysdate() as "current date & time"
from    usuarios;

//special functions
select  email,
        isnull(apellidos)
from    usuarios;

select  email,
        strcmp('hola', 'hola') //returns false(0).
from    usuarios;
select  email,
        strcmp('hola', 'hola2') //returns different(-1).  
from    usuarios;

select  version()
from    usuarios;

select  user()
from    usuarios;
select  distinct user()
from    usuarios;

select  distinct database()
from    usuarios;

select  email,
        ifnull(apellidos, 'Esta campo esta vacio')
from    usuarios;