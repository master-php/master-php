use blog_master;
#insert
insert into usuarios
value(null, 'orlando', 'arias', 'oa@oa.com', '123', '2020-06-19');
insert into usuarios
value(null, 'lina maria', 'arias-bryon', 'lm@lm.com', '123', '2020-02-26');
insert into usuarios
value(null, 'diana jimena', 'arias-bryon', 'dj@dj.com', '123', '2020-06-27');
insert into usuarios
value(null, 'nicholas', 'ocampo', 'no@no.com', '123', '2020-01-04');
insert into usuarios
value(null, 'sophia', 'ocampo', 's@s.com', '123', '2020-09-16');
insert into usuarios
value(null, 'patrick s', 'bach', 'ps@ps.com', '123', '2020-11-28');

#insert specific rows
insert into usuarios(email, password)
values('nn@nn.com', '123');

insert into usuarios
values(null, 'crotatas', 'mochuelo', 'nn@nn.com', '123', '2020-12-15');