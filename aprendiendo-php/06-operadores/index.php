<?php

$numero1 = 55;
$numero2 = 33;

echo 'suma: '.($numero1 + $numero2).'<br/>';
echo 'resta: '.($numero1 - $numero2).'<br/>';
echo 'producto: '.($numero1 * $numero2).'<br/>';
echo 'cociente: '.($numero1 / $numero2).'<br/>';
echo 'modulo: '.($numero1 % $numero2).'<br/>';

$year = 2019;
$year++;
echo '<h1>'.$year.'</h1>'.'<br/>';

$year2 = 2019;
$year2--;
echo '<h1>'.$year2.'</h1>'.'<br/>';

$year3 = 2019;
++$year3;
echo '<h1>'.$year3.'</h1>'.'<br/>';

$year4 = 2019;
--$year4;
echo '<h1>'.$year4.'</h1>'.'<br/>';

//assignments
$edad = 55;
echo $edad.'<br/>';
echo ($edad += 5).'<br/>';
echo ($edad -= 5).'<br/>';
echo ($edad *= 5).'<br/>';
echo ($edad /= 5).'<br/>';