<?php

/* 
 * 1. Debe ser una funcion.
 * 2. la funcion debe validar un e-mail con filter_var.
 * 3. recoger variable por get y validarla.
 * 4. mostrar el resultado.
 */

function validarEmail($email) {
    $status = 'No valido.';
    if (!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $status = 'Valido';
    }    
    return $status;
}

if (isset($_GET['email'])) {
    echo validarEmail($_GET['email']);
} else {
    echo "Pasa por get un email...";
}