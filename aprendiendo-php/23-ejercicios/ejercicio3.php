<?php
/* 
 * hacer una interfaz de usuario (formulario) con dos inputs
 * y 4 botones: sumar, restar, dividir y multiplicar.
 */
$resultado = false;
if(isset($_POST['n1']) &&
   isset($_POST['n2'])) {    
    echo $_POST['n1']."<br/>";
    echo $_POST['n2']."<br/>";
    echo $resultado;
    if(isset($_POST['sumar'])) {
        $resultado = "El resultado es: ".($_POST['n1'] + $_POST['n2']);
    }
    if(isset($_POST['restar'])) {
        $resultado = "El resultado es: ".($_POST['n1'] - $_POST['n2']);
    }
    if(isset($_POST['multiplicar'])) {
        $resultado = "El resultado es: ".($_POST['n1'] * $_POST['n2']);
    }
    if(isset($_POST['dividir'])) {
        $resultado = "El resultado es: ".($_POST['n1'] / $_POST['n2']);
    }
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">        
        <title>Calculadora PHP</title>        
    </head>
    <body>
        <h1>Calculadora PHP</h1>
        <form action="" method="POST">
            <label for="n1">Numero 1: </label> 
            <p><input type="number" name="n1"/></p>
            
            <label for="n2">Numero 2: </label> 
            <p><input type="number" name="n2"/></p>           
                        
            <input type="submit" name="sumar" value="Sumar"/>
            <input type="submit" name="restar" value="Restar"/>
            <input type="submit" name="multiplicar" value="Multiplicar"/>
            <input type="submit" name="dividir" value="Dividir"/>                        
        </form>
        
        <?php
            //resultado
            if($resultado != false) :
                echo "<h2>$resultado</h2>";
            endif;
        ?>
    </body>
</html>


