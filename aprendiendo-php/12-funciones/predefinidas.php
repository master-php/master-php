<?php

//debugging
$nombre = "orlando";
var_dump($nombre);

//fechas-tiempos
echo date('d-M-y').'<br/>';
echo time().'<br/>';

//math
echo "raiz cuadrada de 10: ".sqrt(10).'<br/>'; 
echo "numero aleatoriio entre 10 y 40: ".rand(10, 40).'<br/>'; 
echo "numero pi: ".pi().'<br/>'; 
echo "redondeo: ".round(3.445, 2).'<br/>'; 

//miscellaneous
echo "type variable: ".gettype($nombre).'<br/>'; 

if (is_string($nombre)) {
    echo "esa variable es un string"."<br/>";
}else {
    "wrong type"."<br/>";
}

$texto = ""; //or NULL
if (empty($texto)) {
    echo "la variable esta vacia"."<br/>";
}else {
    echo "la variable tiene contenido"."<br/>";
}

$myString = "Orlando";
echo "la variable tiene: ".strlen($myString)." characters"."<br/>";
echo "la variable tiene character n en la posicion: ".strpos($myString, "n")."<br/>";
echo "la actualizacion es: ".str_replace("r", "R", "$myString")."<br/>";
echo strtolower($myString)."<br/>";
echo strtoupper($myString)."<br/>";