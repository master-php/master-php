<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$frase = "ni los genios son tan genios, ni los mediocres tan mediocres."; //global var.
//echo "<h2>$frase</h2>";

function holaMundo() {
    global $frase;
    
    echo "<h3>$frase</h3>";
    
    $year = 2020;  //local var.
    echo "<h3>$year</h3>";
}        
        
//holaMundo(); 
//echo "<h2>$year</h2>";  //this fails.

//ejemplo con funciones definidas como variable.
function buenosDias() {
   return "hola! buenos dias :)"; 
}

function buenasTardes() {
   return "hey! q tal ha ido la comida?"; 
}

function buenasNoches() {
   return "te vas a dormir ya?  buenos noches!"; 
}

$horario = "buenasNoches";
echo $horario();

//ejemplo con concatenacion
echo '<br/>';
$horario = "Tardes";
$myFunction = "buenas".$horario;

echo $myFunction();
?>
