<?php

/* 
 function syntax:
 * function <function name> (<parameters>) {
 *      <function body>
 * }
 */

//ejemplo 1
function muestraNombres() {
    echo "orlando<br/>";
    echo "lina maria<br/>";
    echo "nicholas<br/>";
    echo "sophia<br/>";
    echo "dian jimena<br/>";
    echo "patrick simon<br/>";    
}

//muestraNombres();

//ejemplo 2
function tabla($numero) {
    for ($k = 1; $k <= 10; $k++) {
        echo "<h3>$numero * $k = ".($k * $numero);
        echo"</h3>";
    }
}

//if (isset($_GET['numero'])) {
//    tabla($_GET['numero']);
//}else {
//    echo "No hay numero para calcular la tabla";
//}

//ejemplo 3
function calculator($numero1, $numero2, $bold = false) {
    $sum = $numero1 + $numero2;
    $sub = $numero1 - $numero2;
    $mul = $numero1 * $numero2;
    $div = $numero1 / $numero2;
    
    if ($bold) {
        echo "<h1>";
    }
    echo "Add: $sum <br/>";
    echo "Sub: $sub <br/>";
    echo "Mul: $mul <br/>";
    echo "Div: $div <br/>";

    if ($bold) {
        echo "</h1>";
    }
}

//calculator(4, 2);
//calculator(10, 5, true);

//ejemplo usando return
function devuelveElNombre($nombre) {
    return "el nombre es: $nombre";
}

//echo devuelveElNombre('crotatas mochuelo');

//ejemplo 3 mejorado (usa return)
function calculator2($numero1, $numero2, $bold = false) {
    $sum = $numero1 + $numero2;
    $sub = $numero1 - $numero2;
    $mul = $numero1 * $numero2;
    $div = $numero1 / $numero2;
    $text_string = "";
    
    if ($bold) {
        $text_string .= "<h1>";
    }
    $text_string .= "Add: $sum <br/>";
    $text_string .= "Sub: $sub <br/>";
    $text_string .= "Mul: $mul <br/>";
    $text_string .= "Div: $div <br/>";

    if ($bold) {
        $text_string .= "</h1>";
    }
    
    return $text_string;
}

//echo calculator2(4, 2);
//echo calculator2(10, 5, true);

//ejemplo 4 embedded functions
function getNombre($nombre) {    
    return "el nombre es: $nombre"
        . "<br/>";
}

function devuelveElNombre2($nombre, $apellidos) {
    $text = "";
    $text .= getNombre($nombre)
          . "<br/>";
    $text .= getApellidos($apellidos);
    return $text;
}

function getApellidos($apellidos) {    
    return "los apellidos son: $apellidos";
}

echo devuelveElNombre2('crotatas', 'mochuelo');
?>