<?php
//sort
$cantantes = ['2pac', 'drake', 'jennifer lopez', 'alfredo'];
$numeros = [3,2,7,4,9];

//asort($cantantes); //for alfa# arrays.
//var_dump($cantantes);

asort($cantantes);//ascending order.
//var_dump($cantantes);

arsort($cantantes); //descending order.
var_dump($cantantes);

sort($numeros);
var_dump($numeros);

//add elementos
$cantantes[] = "crotatas mochuelo";
//array_push($cantantes, 'leo dan');

//remove elements
//array_pop($cantantes);
//unset($cantantes[1]);
//var_dump($cantantes);

//at random access
//echo $cantantes[array_rand($cantantes)].'<br/>';
//echo array_rand($cantantes);

//reverse an array
//var_dump(array_reverse($numeros));

//searches
//echo array_search('drake', $cantantes)."<br/>";
//echo $cantantes[array_search('drake', $cantantes)]."<br/>";

//count elements
//echo count($cantantes)."<br/>";
//echo sizeof($cantantes);
?>