<?php
$peliculas = array('batman', 'spiderman', 'el sr. de los anillos');
//var_dump($peliculas);

$cantantes = ['2pac', 'drake', 'j. lo'];
//var_dump($cantantes);

echo "<h1>Listado de peliculas"."<br/>";
echo "<ul>";
for ($k = 0; $k < count($peliculas); $k++) {
    echo "<li>".$peliculas[$k]."</li>";
}
echo "</ul>";


//working with foreach
echo "<h1>Listado de cantantes"."<br/>";
echo "<ul>";
foreach ($cantantes as $cantante) {
    echo "<li>"."$cantante"."</li>";
}
echo "</ul>";

//associative arrays
$personas = array(
    'nombre' => 'orlando',
    'apellidos' => 'arias',
    'web' => 'oa.com'
);
//var_dump($personas);
//var_dump($personas['web']);

//data sent with GET and POST are associative arrays.

//multidimensioonal arrays
$contactos = array(
        array (
            'nombre' => 'antonio',
            'email' => 'antonio@antonio.com'
        ),
        array (
            'nombre' => 'luis',
            'email' => 'luis@luis.com'
        ),
        array (
            'nombre' => 'salvador',
            'email' => 'salvador@salvador.com'
        )
    );

    //var_dump($contactos);
    //to access Luis:
    echo $contactos[1]['nombre']."<br/>";
    //to access salvador's email:
    echo $contactos[2]['email'];
    
    //listin all names:
    foreach ($contactos as $key => $contacto) {
        var_dump($contacto['nombre']);
    }
?>