<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//crear cookies syntax
//setcookie("nombre", "valor: only text", expiration, path, dominion);

//cookie basica
setcookie("myCookie", "valor de mi galleta");

//cookie with expiration date
setcookie("oneYear", "valor de mi galleta por 365 dias", time() + (60 * 60 * 24 * 365));

header("Location:ver_cookies.php"); //this is a redirection.

?>

