<?php

/* data types:
 * integers: int/integer
 * floating: float/double
 * strings: string
 * booleans: bool
 * nulls: null
 * arrays
 * objects
 */

$numero = 100;
$decimal = 27.9;
$string = 'hola hediondo';
$totallyTrue = true;
$null;
$null2 = null;
$text = "I am a text, $numero".'</br>';
$texta = 'I am a text, $numero'.'</br>';
$text2 = 'I am a text, $numero'; //doesn't work because it needs double quote.
$text3 = "I am a text, \"$numero\"";
$text4 = 'I am a text, "100"';

//echo gettype($numero);
//echo gettype($decimal);
//echo gettype($string);
//echo gettype($totallyTrue);
//echo gettype($null);
// this gives me: Notice: Undefined variable: null in C:\...\index.php on line 22

//echo gettype($null2);
echo $text;
echo $texta;
echo $text3.'</br>';
echo $text4;
//debugging
/*
$myName = 'orlando';
var_dump($myName);

$myName2[] = 'orlando';
$myName2[] = 'orlando';
var_dump($myName2);
 */
?>

