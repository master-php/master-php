<?php

/* 
 render a multiplication table for 1 to 10 using html.  
 */

echo "<table border='1'>"; //inicio de la tabla.
echo "<tr>"; //encabezados
echo "<th>Nombre</th>";
echo "<th>Apellidos</th>";
echo "<th>Pais</th>";
echo "</tr>";

echo "<tr>"; //details
echo "<td>Orlando</td>";
echo "<td>Arias</td>";
echo "<td>Colombia</td>";
echo "</tr>";

echo "<tr>"; //details
echo "<td>Lina Maria</td>";
echo "<td>Arias-Bryon</td>";
echo "<td>Colombia</td>";
echo "</tr>";

echo "<tr>"; //details
echo "<td>Diana Jimena</td>";
echo "<td>Arias-Bryon</td>";
echo "<td>Colombia</td>";
echo "</tr>";
echo "</table>";