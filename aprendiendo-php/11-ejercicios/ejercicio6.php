<?php

 /* 
 render a multiplication table for 1 to 10 using html.  
 */
echo "<table border='1'>"; //inicio de la tabla.
echo "<tr>"; //encabezados
    for ($encabezado = 1; $encabezado <= 10; $encabezado++){
       echo "<th>Tabla del $encabezado</th>"; 
    }
echo "</tr>";

//details

    for ($k = 1; $k <= 10; $k++) {
        echo "<tr>"; //details
        for ($j = 1; $j <= 10; $j++) {
            echo "<td>$j * $k = ".($k * $j);
            echo"</td>";
        }
        echo "</tr>";
    }

echo "</table>";
?>

