<?php

/* 
 listar los cuadrados del los primeros 40 numeros.
 * usar bucle while
 */

//for ($k = 1; $k <= 40; $k++) {
//    $square = $k * $k;
//    echo "<h3>$square<h3>"."<br/>";    
//}

$k = 1;
while ($k <= 40) {
    $square = $k * $k;
    echo "<h3>el cuadrado de $k es $square<h3>"."<br/>";
    $k++;
}