<?php

//while
$numero = 0;
while($numero <= 100){
    echo "$numero";
    if($numero != 100) {
        echo ", ";
    }else {
        echo ".";
    }
    $numero++;        
}

echo '<hr/>';
/*tabla de multiplicar de un numero
 * introducido via GET()
 */
echo "<hr/>";
if(isset($_GET['numero'])) {
    $numero = (int)$_GET['numero']; 
}else {
    $numero = 1;
}

//var_dump($numero);
echo "<h1>Tabla de multiplicar del numero $numero</h1>";

$contador = 1;
while($contador <= 10) {
    echo "$numero * $contador = ".($numero * $contador)."<br/>";
    $contador++;
}
