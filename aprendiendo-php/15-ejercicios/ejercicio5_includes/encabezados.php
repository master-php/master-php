<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8"/>
        <title>Arreglo tabulado</title>
    </head>
    <body>
        <!--cabecera-->
            <h1>Arreglo expresado en forma de tabla</h1>
            <table border="1">
                <tr>
                    <?php foreach($categorias as $categoria): ?>
                    <th><?=$categoria?></th>
                    <?php endforeach; ?>        
                </tr>
