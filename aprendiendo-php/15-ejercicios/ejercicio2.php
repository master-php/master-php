<?php

/* 
 * agregar valores a un arreglo cuando su longitud sea menor de 120.
 * mostrar el arreglo final.
 */
function mostrarArray($numeros) {
    $numString = ""; 
    foreach ($numeros as $numero) {
        $numString .= $numero."<br/>";
    }
    return $numString;
}

$numeros = array();

for ($k = 0; count($numeros) < 120; $k++) {
    array_push($numeros, $k);
}

echo "<h1>Listado de numeros"."<br/>";
echo mostrarArray($numeros);