<?php

/* 
 crear un array con el contenido de la siguiente tabla:
 * Accion   Aventura    Deportes
 * GTA      ASSASINS    FIFA 19
 * COD      CRASH       PES 19
 * PUGB     PRINCE OF   MOTO GP 19  
 *          PERSIA
 * render it in a html table
 * Cada fila va en un fichero separado (include).    
 */

$tabla = array(
    "ACCION" => array ("GTA 5", "Call of Duty", "PUGB"),
    "AVENTURA" => array ("Assasins", "Crash Bandicoot", "Prince of Persia"),
    "DEPORTES" => array ("Fifa 19", "Pes 19", "Moto G 19"));

var_dump($tabla);
$categorias = array_keys($tabla);
var_dump($categorias);
?>

<?php 
require_once "ejercicio5_includes/encabezados.php";
?>

<?php 
require_once "ejercicio5_includes/primera.php";
?>

<?php 
require_once "ejercicio5_includes/segunda.php";
?>

<?php 
require_once "ejercicio5_includes/tercera.php";
?>
