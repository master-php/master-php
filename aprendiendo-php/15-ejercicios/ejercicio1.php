<?php

/* 
 1.Hacer un programa q tenga un array con 8 numeros enteros
 * y q haga lo sgte:
 * recorrerlo y mostrarlo.
 * ordenarlo y mostrarlo.
 * mostrar su longitud.
 * buscar algun elemento.
  */

function mostrarArray($numeros) {
    $numString = ""; 
    foreach ($numeros as $numero) {
        $numString .= $numero."<br/>";
    }
    return $numString;
}
$numeros = array(8, 1, 7, 2, 6, 3, 5, 4);
echo "<h1>Listado de numeros"."<br/>";
//echo "<ul>";
//for ($k = 0; $k < count($numeros); $k++) {
//    echo "<li>".$numeros[$k]."</li>";
//}
//echo "</ul>";
echo mostrarArray($numeros);

echo "<h1>Listado ordenado de numeros"."<br/>";
sort($numeros);
//echo "<ul>";
//for ($k = 0; $k < count($numeros); $k++) {
//    echo "<li>".$numeros[$k]."</li>";
//}
//echo "</ul>";
echo mostrarArray($numeros);

echo "<h1>Longitud del arreglo"."<br/>";
echo sizeOf($numeros)."<br/>";

echo "<h1>Busqueda de un numero"."<br/>";
$busqueda = 10;
if (array_search($busqueda, $numeros)) {
    echo "<h4>el numero buscado existe</h4>";
}else {
    echo "<h4>el numero buscado NO existe</h4>";
}





?>
