<?php

//create a directory
//mkdir('mi_carpeta', 0777) or die('no se puede crear la carpeta.');

//checking if a directory exists.
//if (!is_dir('mi_carpeta')) {
//    mkdir('mi_carpeta', 0777) or die('no se puede crear la carpeta.');
//} else {
//    echo "Ya existe este directorio.";
//}

//delete a directory
//rmdir('mi_carpeta');

//iterating a directory:
if ($gestor = opendir('./mi_carpeta')) {
    while (false !== ($archivo = readdir($gestor))) {//reads a directory entry
        if ($archivo != '.' &&  //this if filters the . and .. directory entries.
            $archivo != '..') {
           echo $archivo."<br/>"; 
        }        
    }
}