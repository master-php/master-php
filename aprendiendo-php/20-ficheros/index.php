<?php

//abrir archivo.
$archivo = fopen("fichero_texto.txt", "a+");
//permissions:
//r: read only
//a+: read and write

//leer contenido
//while (!feof($archivo)) {
//    $contenido = fgets($archivo); //fgets only reads one line.
//    echo $contenido."<br/>";
//}

//escribir 
//fwrite ($archivo, 'Soy un texto metido desde php.');
//cerrar archivo.
//fclose($archivo);

//copy file:
//copy ('fichero_texto.txt', 'copia_del_fichero.txt') or die('error al copiar');

//rename file
//rename('copia_del_fichero.txt', 'just_a_duplicate.txt');

//delete file
//unlink('just_a_duplicate.txt') or die('error al borrar');

//check if the file exists
if (file_exists("fikhero_texto.txt")) {
    echo "el archivo existe!!";
} else {
    echo "NO EXISTE ESTE ARCHIVO!!";
}