<?php
if(isset($_POST['submit'])){
    
    //db connection.
    require_once 'includes/conexion.php';

    session_start();

    
    //recoger los valores del formulario de registro.
    $nombre = isset($_POST['nombre']) ? $_POST['nombre'] : false;
    $apellidos = isset($_POST['apellidos']) ? $_POST['apellidos'] : false;
    $email = isset($_POST['email']) ? $_POST['email'] : false;
    $password = isset($_POST['password']) ? $_POST['password'] : false;
 
    //array de errores
    $errores = array();
    
    //validar los datos antes de guardarlos en la base de datos.
    //validar campo nombre.
    if(!empty($nombre) && 
       !is_numeric($nombre) &&
       !preg_match("/[0-9]/", $nombre)){
        $nombre_validado = true;        
    }else {
        $nombre_validado = false;
        $errores['nombre'] = "el nombre no es valido";
    }
    
    //validar campo apellidos.
    if(!empty($apellidos) && 
       !is_numeric($apellidos) &&
       !preg_match("/[0-9]/", $apellidos)){
        $apellidos_validado = true;        
    }else {
        $apellidos_validado = false;
        $errores['apellidos'] = "los apellidos no son validos";
    }
    
    //validar campo email.
    if(!empty($email) && 
       filter_var($email, FILTER_VALIDATE_EMAIL)){
        $email_validado = true;        
    }else {
        $email_validado = false;
        $errores['email'] = "el email no es valido";
    }
    
    //validar campo de clave de acceso.
    if(!empty($password)) {
        $password_validado = true;        
    }else {
        $password_validado = false;
        $errores['password'] = "la clave esta vacia";
    }
    
    //var_dump($errores);
    $guardar_usuario = false;
    
    if(count($errores) == 0){
        $guardar_usuario = true;
        //encriptar el password.
        $password_segura = password_hash($password, PASSWORD_BCRYPT, ['cost' => 4]);
        
        //var_dump($password);
        //var_dump($password_segura);
        //var_dump(password_verify($password, $password_segura));
        //die();
        //insert record into the db.
        $sql = "insert into usuarios values(null, '$nombre', '$apellidos', '$email', '$password_segura', CURDATE());";
        $guardar = mysqli_query($db, $sql);
        
        //to debug:
        //var_dump(mysqli_error($db));
        //die();
        
        if($guardar){
            $_SESSION['completado'] = "El registro se ha completado con exito.";
        }else{
            $_SESSION['errores']['general'] = "Fallo al guardar el usuario!!";
        }
       
    }else{
        $_SESSION['errores'] = $errores;        
    }
}
header('Location: index.php');


