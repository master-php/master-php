<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Blog de Videojuegos</title>
        <link rel="stylesheet" type="text/css" href="./assets/css/style.css"/>
    </head>
    <body>
        <!--header-->
        <header id="cabecera">
            <div id="logo">
                <a href="index.php">
                    Blog de videojuegos 
                </a>
            </div>
            <!--menu-->
            <nav id="menu">
                <ul>
                    <li>
                        <a href="index.php">Inicio</a>
                    </li>
                    <li>
                        <a href="index.php">Categoria 1</a>
                    </li>
                    <li>
                        <a href="index.php">Categoria 2</a>
                    </li>
                    <li>
                        <a href="index.php">Categoria 3</a>
                    </li>
                    <li>
                        <a href="index.php">Categoria 4</a>
                    </li>
                    <li>
                        <a href="index.php">Sobre mi</a>
                    </li>
                    <li>
                        <a href="index.php">Contacto</a>
                    </li>
                </ul>
            </nav>
            <div class="clearfix"/>
        </header>
        
        <div id="container">
            <!--sidebar-->
            <aside id="sidebar">
                <div id="login" class="bloque">
                    <h3>Identificate</h3>
                    <form action="login.php" method="POST">
                        <label for="email">Email</label>
                        <input type="email" name="email"/>
                        <label for="password">Contrasena</label>
                        <input type="password" name="password"/>
                        <input type="submit" value="Entrar"/>
                    </form>
                </div>
                
                <div id="register" class="bloque">
                    <h3>Registrate</h3>
                    <form action="registro.php" method="POST">
                        <label for="nombre">Nombre</label>
                        <input type="text" name="nombre"/>
                        <label for="apellidos">Apellidos</label>
                        <input type="text" name="apellidos"/>
                        <label for="email">Email</label>
                        <input type="email" name="email"/>
                        <label for="password">Contrasena</label>
                        <input type="password" name="password"/>
                        <input type="submit" name="submit" value="Registrar"/>
                    </form>
                </div>
            </aside>

            <!--main content-->
            <div id="principal">
                <h1>Ultimas entradas</h1>
                <article class="entrada">
                    <a href="">
                        <h2>Titulo de mi entrada</h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                            Etiam orci est, dapibus at elit ac, lacinia lobortis libero.                   
                        </p>
                    </a>
                </article>
                <article class="entrada">
                    <a href="">
                        <h2>Titulo de mi entrada</h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                            Etiam orci est, dapibus at elit ac, lacinia lobortis libero.                   
                        </p>
                    </a>
                </article>
                    <article class="entrada">
                        <a href="">
                            <h2>Titulo de mi entrada</h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                Etiam orci est, dapibus at elit ac, lacinia lobortis libero.                   
                            </p>
                        </a>
                </article>
                <article class="entrada">
                    <a href="">
                        <h2>Titulo de mi entrada</h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                            Etiam orci est, dapibus at elit ac, lacinia lobortis libero.                   
                        </p>
                    </a>
                </article>

                <div id="ver-todas">
                    <a href="">Ver todas las entradas</a>
                </div>
            </div> <!--fin principal--> 
        </div> <!--end of container-->  
        
        <!--footer-->
        <footer id="pie">
            <p>Desarrollado por Orlando Arias &copy; 2021</p>
        </footer>
    </body>

</html>

<?php // require_once 'includes/cabecera.php'; ?>
        
<?php //require_once 'includes/lateral.php'; ?>
            
<?php //require_once 'includes/pie.php'; ?>

